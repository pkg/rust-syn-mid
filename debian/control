Source: rust-syn-mid
Section: rust
Priority: optional
Build-Depends: debhelper (>= 11),
 dh-cargo (>= 18),
 cargo:native <!nocheck>,
 rustc:native <!nocheck>,
 libstd-rust-dev <!nocheck>,
 librust-proc-macro2-1+default-dev <!nocheck>,
 librust-quote-1+default-dev <!nocheck>,
 librust-syn-1+derive-dev (>= 1.0.5-~~) <!nocheck>,
 librust-syn-1+parsing-dev (>= 1.0.5-~~) <!nocheck>,
 librust-syn-1+printing-dev (>= 1.0.5-~~) <!nocheck>
Maintainer: Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
Uploaders:
 Daniel Kahn Gillmor <dkg@fifthhorseman.net>
Standards-Version: 4.4.1
Vcs-Git: https://salsa.debian.org/rust-team/debcargo-conf.git [src/syn-mid]
Vcs-Browser: https://salsa.debian.org/rust-team/debcargo-conf/tree/master/src/syn-mid
Homepage: https://github.com/taiki-e/syn-mid

Package: librust-syn-mid-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-proc-macro2-1+default-dev,
 librust-quote-1+default-dev,
 librust-syn-1+derive-dev (>= 1.0.5-~~),
 librust-syn-1+parsing-dev (>= 1.0.5-~~),
 librust-syn-1+printing-dev (>= 1.0.5-~~)
Suggests:
 librust-syn-mid+clone-impls-dev (= ${binary:Version})
Provides:
 librust-syn-mid+default-dev (= ${binary:Version}),
 librust-syn-mid-0-dev (= ${binary:Version}),
 librust-syn-mid-0+default-dev (= ${binary:Version}),
 librust-syn-mid-0.5-dev (= ${binary:Version}),
 librust-syn-mid-0.5+default-dev (= ${binary:Version}),
 librust-syn-mid-0.5.0-dev (= ${binary:Version}),
 librust-syn-mid-0.5.0+default-dev (= ${binary:Version})
Description: Providing the features between "full" and "derive" of syn - Rust source code
 This package contains the source for the Rust syn-mid crate, packaged by
 debcargo for use with cargo and dh-cargo.

Package: librust-syn-mid+clone-impls-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-syn-mid-dev (= ${binary:Version}),
 librust-syn-1+clone-impls-dev (>= 1.0.5-~~)
Provides:
 librust-syn-mid-0+clone-impls-dev (= ${binary:Version}),
 librust-syn-mid-0.5+clone-impls-dev (= ${binary:Version}),
 librust-syn-mid-0.5.0+clone-impls-dev (= ${binary:Version})
Description: Providing the features between "full" and "derive" of syn - feature "clone-impls"
 This metapackage enables feature "clone-impls" for the Rust syn-mid crate, by
 pulling in any additional dependencies needed by that feature.
